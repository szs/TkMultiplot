TkMultiplot
===========

API to plot multiple figures on a `Tkinter.Canvas`. The individual subplots
are arranged in a grid and addressed by a tuple: (n_row,n_column)

The widget is called `TkMultiplot` and is derived from a `Tkinter.Canvas`.
Therefore you can call every method and set every parameter as you would
in a `Tkinter.Canvas`.
Additionally, `TkMultiplot` implements a number of methods and parameters
described in section **API**.

Usage example
-------------

    self.canvas = TkMultiplot(parent, bg='#eef',
                              columns = 2, rows = 3,
                              width=1000, height=600)

    xval = -np.arange(200)/15. # arange from numpy

    self.canvas.plot((0,0), xval, np.sin(xval), bg='#afa')
    self.canvas.plot((1,0), xval, np.sinh(xval/10), bg='#aaf')
    self.canvas.plot((2,0), xval, (xval-1)**2)


API
---

**class `TkMultiplot`**

*description:*

Widget to draw multiple subplots onto a Canvas.

*parent class:*

`Tkinter.Canvas`

*methods:*


    TkMultiplot(parent,                     # parent widget
                columns=1,                  # number of columns
                rows=1,                     # number of rows
                ipadt=20,                   # internal padding at the top of each subplot
                ipadb=20,                   # internal padding at the bottom of each subplot
                ipadl=45,                   # internal padding at the left of each subplot
                ipadr=30,                   # internal padding at the right of each subplot
                xticksize=7,                # size of ticks on x-axis
                yticksize=7,                # size of ticks on y-axis
                mtickfactor=2.0,            # factor of major ticks relative to other ticks
                which_axes=Tkinter.NSEW,    # which axes to draw
                axes_colour='#000'          # default colour for axes
                args*, kwargs**)            # all other args are passed to the Tkinter.Canvas constructor

    plot(subplot=(0,0),                     # tuple indicating which subplot to draw to
         xval=[],                           # array of x-values
         yval=[],                           # array of y-values
         bg='#fff',                         # background for subplot
         linestyle='k-',                    # set the linestyle
         linewidth=1.0,                     # set the linewidth
         overlay=False,                      # if True, no background, axes, ticks and labels are drawn
         axes_colour='#000'                 # axes colour for subplot
         )

    set_rowscolumns(r=1,                    # number of rows
                    c=1):                   # number of columns

**global function `frange`**

*description:*

Similar to python's built-in function range, but works with `float`.

*function:*

    frange(start,                           # start value
           stop,                            # stop value (exclusive)
           step,                            # step value
           precision=None)                  # if given returns a rounded value
