#!/usr/bin/env python
import Tkinter as tk
from math import log10, floor, ceil

def frange(start, stop, step, precision=None):
    while start < stop:
        yield start
        start += step
        if precision:
            start=round(start, int(round(precision)))

class TkMultiplot(tk.Canvas):
    def __init__(self, *args, **kwargs):

        # number of rows and columns
        self.columns = kwargs.pop('columns', 1)
        self.rows = kwargs.pop('rows', 1)

        # padding for subplot axes
        self.ipadt = kwargs.pop('ipadt', 20)
        self.ipadb = kwargs.pop('ipadb', 20)
        self.ipadl = kwargs.pop('ipadl', 45)
        self.ipadr = kwargs.pop('ipadr', 30)

        # tick parameters
        self.xticksize = kwargs.pop('xticksize', 7)
        self.yticksize = kwargs.pop('yticksize', 7)
        self.mtickfactor = kwargs.pop('mtickfactor', 2.0)

        # which axes should be plotted
        self.which_axes = tk.NSEW

        # colour parameters
        self.axes_colour = kwargs.pop('axes_colour', None)
        if self.axes_colour == None:
            self.axes_colour = kwargs.pop('axes_color', '#000')

        if self.axes_colour[0] != '#':
            self.axes_colour = self._col_let2num(self.axes_colour)

        tk.Canvas.__init__(self, *args, **kwargs)

    def _col_let2num(self, let):
        if let == 'r' or let == 'red':
            return '#f00'
        if let == 'g' or let == 'green':
            return '#0f0'
        if let == 'b' or let == 'blue':
            return '#00f'
        if let == 'y' or let == 'yellow':
            return '#ff0'
        if let == 'c' or let == 'cyan':
            return '#0ff'
        if let == 'm' or let == 'magenta':
            return '#f0f'
        if let == 'k' or let == 'black':
            return '#000'
        if let == 'w' or let == 'white':
            return '#fff'
        return None

    def _col_num2let(self,num):
        if num == '#f00':
            return 'r'
        if num == '#0f0':
            return 'g'
        if num == '#00f':
            return 'b'
        if num == '#ff0':
            return 'y'
        if num == '#0ff':
            return 'c'
        if num == '#f0f':
            return 'm'
        if num == '#000':
            return 'k'
        if num == '#fff':
            return 'w'
        return None

    def plot(self, subplot=(0,0), xval=[], yval=[],
             bg=None, axes_bg=None, linestyle='k-', linewidth=1.0,
             overlay=False, axes_colour=None,
             xmin=None, xmax=None, ymin=None, ymax=None):
        assert subplot[0] < self.rows
        assert subplot[1] < self.columns

        if axes_colour == None:
            axes_colour = self.axes_colour

        if axes_colour[0] != '#':
            axes_colour = self._col_let2num(axes_colour)

        if bg == None:
            bg = self.cget('bg')

        if bg[0] != '#':
            bg = self._col_let2num(bg)

        if axes_bg == None:
            axes_bg = bg

        if axes_bg[0] != '#':
            axes_bg = self._col_let2num(axes_bg)

        self.update_idletasks()

        # dimensions of whole canvas
        sr = [int(x) for x in self.cget('scrollregion').split()]
        if len(sr) != 0:
            w = sr[2]-sr[0]
            h = sr[3]-sr[1]
        else:
            h = self.winfo_height()
            w = self.winfo_width()

        # dimensions of single plot
        s_h = h / self.rows
        s_w = w / self.columns

        # bounding coordinates of specified subplot in canvas dimensions
        t,b,l,r = ( subplot[0]*s_h,
                    (subplot[0]+1)*s_h,
                    subplot[1]*s_w,
                    (subplot[1]+1)*s_w)

        # axes bounding coordinates
        at, ab, al, ar = t + self.ipadt, b - self.ipadb, l + self.ipadl, r - self.ipadr
        atbspan = at-ab
        arlspan = ar-al

        if not overlay:

            # calculate some useful numbers
            if xval != None:
                if xmin == None:
                    xmin = min(xval)
                if xmax == None:
                    xmax = max(xval)
            if yval != None:
                if ymin == None:
                    ymin = min(yval)
                if ymax == None:
                    ymax = max(yval)

            xspan = xmax - xmin
            assert xspan > 0
            yspan = ymax - ymin
            assert yspan > 0

            # calculate ticks
            tick_corr = 0.2

            xtickdelta = 10**(round(log10(xspan)-tick_corr) - 1)
            self.xtickmin = floor(xmin/xtickdelta) * xtickdelta
            xtickmax = ceil(xmax/xtickdelta) * xtickdelta
            xtickspan = xtickmax - self.xtickmin

            xticks = list(frange(self.xtickmin, xtickmax+xtickdelta, xtickdelta, precision=max(0,-round(log10(xspan))+2)))
            self.ahori_factor = arlspan / xtickspan

            ytickdelta = 10**(round(log10(yspan)-tick_corr) - 1)
            self.ytickmin = floor(ymin/ytickdelta) * ytickdelta
            ytickmax = ceil(ymax/ytickdelta) * ytickdelta
            ytickspan = ytickmax - self.ytickmin

            yticks = list(frange(self.ytickmin, ytickmax+ytickdelta, ytickdelta, precision=max(0,-round(log10(yspan))+2)))
            self.avert_factor = atbspan / ytickspan

            # set background
            self.create_rectangle(l,t,r,b, fill=bg)
            self.create_rectangle(al,at,ar,ab, fill=axes_bg)

            # plot axes
            if tk.N in self.which_axes:
                self.create_line(al, at, ar, at, fill=axes_colour)
            if tk.S in self.which_axes:
                self.create_line(al, ab, ar, ab, fill=axes_colour)
            if tk.E in self.which_axes:
                self.create_line(ar, at, ar, ab, fill=axes_colour)
            if tk.W in self.which_axes:
                self.create_line(al, at, al, ab, fill=axes_colour)

            # plot ticks
            for xt in xticks:
                xtmp = al + self.ahori_factor * (xt - self.xtickmin)
                if (xt == 0.0 or
                    xt % 10**(round(log10(xspan)-tick_corr)) == 0.0):
                    factor = self.mtickfactor
                else:
                    factor = 1.0
                self.create_line( xtmp, ab, xtmp, ab-self.xticksize*factor, fill=axes_colour)

            for yt in yticks:
                ytmp = ab + self.avert_factor * (yt - self.ytickmin)
                if (yt == 0.0 or
                    yt % 10**(round(log10(yspan)-tick_corr)) == 0.0):
                    factor = self.mtickfactor
                else:
                    factor = 1.0
                self.create_line( al, ytmp, al+self.yticksize*factor, ytmp, fill=axes_colour)

            # plot labels
            if xticks:
                self.create_text(al, ab+0.5*self.ipadb, text='%.3f'%self.xtickmin, fill=axes_colour)
                self.create_text(ar, ab+0.5*self.ipadb, text='%.3f'%xtickmax, fill=axes_colour)
            if yticks:
                self.create_text(al-0.5*self.ipadl, ab, text='%.3f'%self.ytickmin, fill=axes_colour)
                self.create_text(al-0.5*self.ipadl, at, text='%.3f'%ytickmax, fill=axes_colour)

        # plot data

        if xval == None:
            data = [(al, ab + self.avert_factor * (yval - self.ytickmin)) , (ar, ab + self.avert_factor * (yval - self.ytickmin))]
        elif yval == None:
            data = [(al + self.ahori_factor * (xval - self.xtickmin), ab) , (al + self.ahori_factor * (xval - self.xtickmin), at)]
        else:
            data = [(al + self.ahori_factor * (x - self.xtickmin),ab + self.avert_factor * (y - self.ytickmin)) for x,y in zip(xval, yval)]

        if 'k' in linestyle:
            line_colour = 'black'
        elif 'b' in linestyle:
            line_colour = 'blue'
        elif 'g' in linestyle:
            line_colour = 'green'
        elif 'r' in linestyle:
            line_colour = 'red'
        elif 'c' in linestyle:
            line_colour = 'cyan'
        elif 'm' in linestyle:
            line_colour = 'magenta'
        elif 'y' in linestyle:
            line_colour = 'yellow'
        elif 'w' in linestyle:
            line_colour = 'white'
        elif '#' in linestyle:
            line_colour = linestyle[linestyle.index('#'):linestyle.index('#')+4]
        else:
            line_colour = '#000'

        if '--' in linestyle:
            dash = (7,)
        elif '-.' in linestyle or '.-' in linestyle:
            dash = (7,3,1,3)
        elif '-' in linestyle:
            dash = None
        elif ':' in linestyle:
            dash = (1,4)
        else:
            dash = None

        self.create_line(data, width=linewidth, fill=line_colour, dash=dash)

    def set_rowscolumns(self, r=1, c=1):
        self.rows = r
        self.columns = c

if __name__=='__main__':

    from numpy import arange
    import numpy as np

    class Application(tk.Frame):
        N_COLS = 2
        N_ROWS = 3
        def __init__(self, master=None):
            tk.Frame.__init__(self, master)
            self.master = master
            self.grid()
            self.createWidgets()

            self.plot()

        def createWidgets(self):

            self.frame = tk.Frame(self)
            self.frame.grid(column = 0, columns=2, row = 0, sticky=tk.NSEW)

            vbar = tk.Scrollbar(self.frame,orient=tk.VERTICAL)
            vbar.grid(column=1, row=0, sticky=tk.NS)

            self.canvas = TkMultiplot(self.frame, bg='#eef', axes_colour='r',
                                      columns = self.N_COLS, rows = self.N_ROWS,
                                      width=400, height=400, scrollregion=(0,0,400,1000))
            #~ self.canvas.config(width=400, height=400) # instead of constructor param

            self.canvas.grid(column = 0, row = 0, sticky=tk.NSEW)

            vbar.config(command=self.canvas.yview)
            self.canvas.config(yscrollcommand=vbar.set)

            self.plotButton = tk.Button(self, text='Plot', command=self.plot)
            self.plotButton.grid(column = 0, row = 1, sticky=tk.EW)
            self.quitButton = tk.Button(self, text='Quit', command=self.master.destroy)
            self.quitButton.grid(column = 1, row = 1, sticky=tk.EW)

        def plot(self):

            xval = -arange(200)/19.

            self.canvas.plot((0,0), xval, np.sin(xval), bg='#afa')
            self.canvas.plot((0,0), 0.6, None, overlay=True, linestyle='#af0')
            self.canvas.plot((0,0), None, -7.5, overlay=True, linestyle='#fa0')
            self.canvas.plot((1,0), xval, np.sinh(xval/10), bg='#aaf', linestyle='r:')
            self.canvas.plot((2,0), xval, (xval-1)**2, linestyle='m-.', linewidth=2.0)
            self.canvas.plot((0,1), 0.7, None, bg='#ffa', linestyle='b.-', xmin=0.0, xmax=1.0, ymin=-1.0, ymax=2.5)
            self.canvas.plot((0,1), None, 0.7, linestyle='b.-', overlay=True)
            self.canvas.plot((1,1), xval, np.cosh(xval/10), bg='#000', linestyle='y--', axes_colour='y', linewidth=4.0)
            self.canvas.plot((2,1), xval, (xval-1)**3-xval**2, bg='#faa',linestyle='g-', axes_bg='#88f', ymin=-1700, ymax=100)
            self.canvas.plot((2,1), xval, (-xval+1)**2.9-10*xval**2, bg='#faa',linestyle='w-', overlay=True)


    root = tk.Tk()
    app = Application(root)
    app.master.title('TkMultiplot Test Application')
    root.mainloop()
